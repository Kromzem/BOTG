import java.util.*

var myTeam: Int = 0

fun main(args: Array<String>) {
    val input = Scanner(System.`in`)
    myTeam = input.nextInt()

    System.err.println(myTeam)

    val bushAndSpawnPointCount = input.nextInt() // usefrul from wood1, represents the number of bushes and the number of places where neutral units can spawn
    for (i in 0 until bushAndSpawnPointCount) {
        val entityType = input.next() // BUSH, from wood1 it can also be SPAWN
        val x = input.nextInt()
        val y = input.nextInt()
        val radius = input.nextInt()
    }

    val itemCount = input.nextInt() // useful from wood2
    for (i in 0 until itemCount) {
        val itemName = input.next() // contains keywords such as BRONZE, SILVER and BLADE, BOOTS connected by "_" to help you sort easier
        val itemCost = input.nextInt() // BRONZE items have lowest cost, the most expensive items are LEGENDARY
        val damage = input.nextInt() // keyword BLADE is present if the most important item stat is damage
        val health = input.nextInt()
        val maxHealth = input.nextInt()
        val mana = input.nextInt()
        val maxMana = input.nextInt()
        val moveSpeed = input.nextInt() // keyword BOOTS is present if the most important item stat is moveSpeed
        val manaRegeneration = input.nextInt()
        val isPotion = input.nextInt() // 0 if it's not instantly consumed
    }

    val myUnits = arrayListOf<Entity>()
    val enemyUnits = arrayListOf<Entity>()

    var hero: Entity? = null
    var enemy: Entity? = null

    var tower: Entity? = null
    var enemyTower: Entity? = null


    // game loop
    while (true) {
        myUnits.clear()
        enemyUnits.clear()

        val gold = input.nextInt()
        val enemyGold = input.nextInt()
        val roundType = input.nextInt() // a positive value will show the number of heroes that await a command
        val entityCount = input.nextInt()
        for (i in 0 until entityCount) {
            val unitId = input.nextInt()
            val team = input.nextInt()
            val unitType = UnitType.valueOf(input.next()) // UNIT, HERO, TOWER, can also be GROOT from wood1
            val x = input.nextInt()
            val y = input.nextInt()
            val attackRange = input.nextInt()
            val health = input.nextInt()
            val maxHealth = input.nextInt()
            val shield = input.nextInt() // useful in bronze
            val attackDamage = input.nextInt()
            val movementSpeed = input.nextInt()
            val stunDuration = input.nextInt() // useful in bronze
            val goldValue = input.nextInt()
            val countDown1 = input.nextInt() // all countDown and mana variables are useful starting in bronze
            val countDown2 = input.nextInt()
            val countDown3 = input.nextInt()
            val mana = input.nextInt()
            val maxMana = input.nextInt()
            val manaRegeneration = input.nextInt()
            val heroType = input.next() // DEADPOOL, VALKYRIE, DOCTOR_STRANGE, HULK, IRONMAN
            val isVisible = input.nextInt() // 0 if it isn't
            val itemsOwned = input.nextInt() // useful from wood1

            val entity = Entity(
                    unitId,
                    team,
                    unitType,
                    attackRange,
                    attackDamage,
                    movementSpeed,
                    maxHealth,
                    health,
                    maxMana,
                    mana,
                    Vector2(x, y)
            )

            when (unitType) {
                UnitType.HERO -> {
                    if (team == myTeam) {
                        hero = entity
                    } else {
                        enemy = entity
                    }
                }

                UnitType.UNIT -> {
                    if (team == myTeam) {
                        myUnits.add(entity)
                        System.err.println("My unit")
                    } else {
                        enemyUnits.add(entity)
                    }
                }

                UnitType.TOWER -> {
                    if (team == myTeam) {
                        tower = entity
                        System.err.println("My tower")
                    } else {
                        enemyTower = entity
                    }
                }

                UnitType.GROOT -> {
                }
            }
        }

        // Write an action using println()
        // To debug: System.err.println("Debug messages...");


        // If roundType has a negative value then you need to output a Hero name, such as "DEADPOOL" or "VALKYRIE".
        // Else you need to output roundType number of any valid action, such as "WAIT" or "ATTACK unitId"

        if (roundType < 0) {
            println(HeroData.IRONMAN)
        } else {
            if (hero != null) {
                if (myUnits.isEmpty()) {
                    move(tower?.position)
                } else {
                    myUnits.sortBy { it.position.dst(tower?.position) }

                    if ((myTeam == 0 && hero.position.x + hero.attackRange * 0.25f < myUnits.last().position.x) ||
                            (myTeam == 1 && hero.position.x - hero.attackRange * 0.25f > myUnits.last().position.x)) {
                        if (enemy != null && hero.position.dst(enemy.position) < hero.attackRange) {
                            attack(enemy.unitID)
                        } else if (!enemyUnits.isEmpty()) {
                            attackNearest(UnitType.UNIT)
                        } else if (enemy != null) {
                            attackNearest(UnitType.HERO)
                        } else {
                            attackNearest(UnitType.TOWER)
                        }
                    } else {
                        if (myTeam == 0) {
                            move(hero.position.x - 100, hero.position.y)
                        } else {
                            move(hero.position.x + 100, hero.position.y)
                        }
                    }
                }
            } else {
                doNothing()
            }
        }
    }
}

fun doNothing() {
    println("WAIT")
}

fun move(position: Vector2?) {
    if (position == null) {
        doNothing()
        return
    }

    move(position.x, position.y)
}

fun move(x: Int, y: Int) {
    println("MOVE $x $y")
}

fun attack(otherUnitID: Int) {
    println("ATTACK $otherUnitID")
}

fun attackNearest(otherUnitType: UnitType) {
    println("ATTACK_NEAREST $otherUnitType")
}

fun moveAndAttack(position: Vector2, otherUnitID: Int) {
    moveAndAttack(position.x, position.y, otherUnitID)
}

fun moveAndAttack(x: Int, y: Int, otherUnitID: Int) {
    println("MOVE_ATTACK $x $y $otherUnitID")
}

data class Entity(
        val unitID: Int,
        val team: Int,
        val type: UnitType,
        val attackRange: Int,
        val damage: Int,
        val moveSpeed: Int,
        val maxHealth: Int,
        var health: Int,
        val maxMana: Int,
        var mana: Int,
        val position: Vector2
)

enum class UnitType {
    UNIT,
    HERO,
    TOWER,
    GROOT
}

enum class HeroData(
        val health: Int,
        val mana: Int,
        val damage: Int,
        val moveSpeed: Int,
        val manaRegen: Int,
        val attackRange: Int
) {
    DEADPOOL(
            1380,
            100,
            80,
            200,
            1,
            110
    ),

    DOCTOR_STRANGE(
            955,
            300,
            50,
            200,
            2,
            245
    ),

    HULK(
            1450,
            90,
            80,
            200,
            1,
            95
    ),

    IRONMAN(
            820,
            200,
            60,
            200,
            2,
            270
    ),

    VALKYRIE(
            1400,
            155,
            65,
            200,
            2,
            130
    )
}

data class Vector2(var x: Int, var y: Int) {
    fun dst(otherVector: Vector2?): Int {
        if (otherVector == null) return Int.MAX_VALUE

        return Math.sqrt(Math.pow((x - otherVector.x).toDouble(), 2.0) + Math.pow((y - otherVector.y).toDouble(), 2.0)).toInt()
    }

    fun set(x: Int, y: Int) {
        this.x = x
        this.y = y
    }

    override fun toString(): String {
        return "[x = $x | y = $y]"
    }
}